import sys;
numeralObjects = [
	['M',	1000],
	['CM',	900],
	['D', 	500],
	['CD',	400],
	['C',	100],
	['XC',	90],
	['L',	50],
	['XL',	40],
	['X',	10],
	['IX',	9],
	['V',	5],
	['IV',	4],
	['I',	1]
];
NUMERAL=0; DECIMAL=1;
def getNumeral(decimal, numeral=None):
	if numeral is None:
		numeral = '';
	if decimal == 0:
		return numeral;
	obj = getNumeralObject(decimal);
	return getNumeral(decimal-obj[DECIMAL],numeral+obj[NUMERAL]);
def getNumeralObject(decimal):
	for obj in numeralObjects:
		if decimal >= obj[DECIMAL]:
			return obj;
	return ['',0];
for i in range(1,len(sys.argv)):
	print(sys.argv[i]+' in roman numerals is ' + getNumeral(int(sys.argv[i])));